# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.

class Animal (models.Model):
	name = models.CharField(u'name', max_length=2300)
	sound = models.CharField(u'sound', max_length=2300)
