 FROM python:3
 ENV PYTHONUNBUFFERED 1
 RUN mkdir /composeexample
 WORKDIR /composeexample
 ADD requirements.txt /composeexample/
 RUN pip install -r requirements.txt
 RUN apt-get update
 RUN apt-get install -y postgresql
 ADD . /composeexample/